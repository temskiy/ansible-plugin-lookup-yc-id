from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.plugins.lookup import LookupBase
from ansible.errors import AnsibleError
import yandexcloud
from yandex.cloud.compute.v1.instance_service_pb2 import ListInstancesRequest
from yandex.cloud.resourcemanager.v1.cloud_service_pb2 import ListCloudsRequest
from yandex.cloud.resourcemanager.v1.folder_service_pb2 import ListFoldersRequest
from yandex.cloud.compute.v1.instance_service_pb2_grpc import InstanceServiceStub
from yandex.cloud.resourcemanager.v1.cloud_service_pb2_grpc import CloudServiceStub
from yandex.cloud.resourcemanager.v1.folder_service_pb2_grpc import FolderServiceStub
from google.protobuf.json_format import MessageToDict

class LookupModule(LookupBase):

    sdk = yandexcloud.SDK()

    def run(self, terms, variables, **kwargs):
        if (not  'service_account_key' in kwargs) and (not 'token' in kwargs):
            raise AnsibleError('Service account key or token must be provided')
        elif ('service_account_key' in kwargs):
            self.initSDK(sa_key=kwargs['service_account_key'])
        elif ('token' in kwargs):
            self.initSDK(token=kwargs['token'])
        ret = []
        if 'cloud_name' in kwargs:
            cloud_id = self.cloud_id_by_name(kwargs['cloud_name'])
        elif 'cloud_id' in kwargs:
            cloud_id = kwargs['cloud_id']
        ret = [cloud_id]
        
        if 'folder_id' in kwargs:
            folder_id = kwargs['folder_id']
        elif (('folder_name' in kwargs) and (cloud_id is not None)):
            folder_id = self.folder_id_by_name(kwargs['folder_name'],cloud_id)
        
        if folder_id is not None:
            ret = [folder_id]
        
        if ('instance_name' in kwargs) and (folder_id is not None):
            instance_id = self.instance_id_by_name(kwargs['instance_name'],folder_id)
            ret = [instance_id]
        return ret

    def initSDK(self,sa_key=None, token=None):
        if sa_key is not None:
            self.sdk = yandexcloud.SDK(service_account_key=sa_key)
        elif token is not None:
            self.sdk = yandexcloud.SDK(token=token)

    def get_id_by_name(self, elements, name):
        for element in elements:
            if element['name'] == name:
                    return element['id']
        return None

    def cloud_id_by_name(self, name):
        cloud_service = self.sdk.client(CloudServiceStub)
        clouds = cloud_service.List(ListCloudsRequest())
        cloud_id = self.get_id_by_name(MessageToDict(clouds)['clouds'], name)
        return cloud_id

    def folder_id_by_name(self, name, cloud_id):
        folder_service = self.sdk.client(FolderServiceStub)
        folders = folder_service.List(ListFoldersRequest(cloud_id=cloud_id))
        folder_id = self.get_id_by_name(MessageToDict(folders)['folders'], name)
        return folder_id

    def instance_id_by_name(self, name, folder_id):
        instance_service = self.sdk.client(InstanceServiceStub)
        instances = instance_service.List(ListInstancesRequest(folder_id=folder_id))
        instance_id = self.get_id_by_name(MessageToDict(instances)['instances'], name)
        return instance_id