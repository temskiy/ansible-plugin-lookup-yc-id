Lookup-плагин для получения id ресурса в яндекс-облаке
======================================================

Возвращает id облака/папки/виртуалки по имени ресурса.

1. Если задан только cloud_name вернёт cloud_id
2. Если задан cloud_name/cloud_id и folder_name вернёт folder_id
3. Если заданы cloud_name/cloud_id и folder_name/folder_id и instance_name вернёт instance_id

```yml
vars:
  folder: "{{ lookup('yc_id_by_name', cloud_name='mycloud', folder_name='test-kuber', service_account_key=sa_key) }}"

```

Для авторизации можно использовать либо токен, либо авторизованый ключ сервисного аккаунта.
Авторизованый ключ имеет такую структуру:

```yml
 sa_key:
      id: "jkhfajksdfhaf"
      service_account_id: "dsfjklsdjfslkdj"
      private_key: "-----BEGIN PRIVATE KEY--**************---PRIVATE KEY-----"
```

С токеном всё гораздо проще:

```yml
token: "jhhasdfhakwioqu"
```